@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Clients Profile List</h3>
                </div>
                <div class="card-body">
                    <table class="table">
                        <caption>Clients Profile</caption>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Nationality</th>
                                <th>Date of Birth</th>
                                <th>Education</th>
                                <th>Mode of Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($profiles as $profile)
                                <tr>
                                @for ($i = 0; $i < 8; $i++)
                                    <td>{{ $profile[$i] }}</td>
                                @endfor
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
