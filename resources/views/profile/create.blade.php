@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>Profile</h3>
                </div>
                    {{ Form::open(['method' => 'post', 'route' => 'profile.store', 'id' => 'profile-form']) }}
                <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                            <div class="col-md-6">
                                {{ Form::text('name', null, ['class' => "form-control"]) }}

                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Gender</label>
                            <div class="col-md-6">
                                Male {{ Form::radio('gender', 'M') }} 
                                Female {{ Form::radio('gender', 'F') }} 
                                @if ($errors->has('gender'))
                                    <span class="text-danger">{{ $errors->first('gender') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                            <div class="col-md-6">
                                {{ Form::text('phone', null, ['class' => 'form-control']) }}
                                @if ($errors->has('phone'))
                                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>
                            <div class="col-md-6">
                                {{ Form::text('address', null, ['class' => 'form-control']) }}
                                @if ($errors->has('address'))
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nationality" class="col-md-4 col-form-label text-md-right">Nationality</label>
                            <div class="col-md-6">
                                {{ Form::text('nationality', null, ['class' => 'form-control']) }}
                                @if ($errors->has('nationality'))
                                    <span class="text-danger">{{ $errors->first('nationality') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date_of_birth" class="col-md-4 col-form-label text-md-right">Date of Birth</label>
                            <div class="col-md-6">
                                {{ Form::date('date_of_birth', null, ['class' => 'form-control']) }}
                                @if ($errors->has('date_of_birth'))
                                    <span class="text-danger">{{ $errors->first('date_of_birth') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="education" class="col-md-4 col-form-label text-md-right">Education</label>
                            <div class="col-md-6">
                                {{ Form::textarea('education', null, ['class' => 'form-control']) }}
                                @if ($errors->has('education'))
                                    <span class="text-danger">{{ $errors->first('education') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mode_of_contact" class="col-md-4 col-form-label text-md-right">Mode of Contact</label>
                            <div class="col-md-6">
                                {{ Form::select('mode_of_contact', ['Email', 'Phone', 'None'], null, ['class' => 'form-control', 'placeholder' => '--select mode of contact--']) }}
                                @if ($errors->has('mode_of_contact'))
                                    <span class="text-danger">{{ $errors->first('mode_of_contact') }}</span>
                                @endif
                            </div>
                        </div>
                </div>
                <div class="card-footer">
                    {{ Form::submit('Submit', ['class' => 'btn btn-primary float-right']) }}
                </div>
                    {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@endsection

@section ('after-scripts')

    <script type="text/javascript">
        $(document).ready( function () {
            $('#profile-form').validate({
                errorClass: "text-danger",
                rules : {
                    name: "required",
                    phone: {
                        required: true,
                        numeric: true
                    },
                    address: {
                        required: true,
                        maxlength: 150
                    },
                    nationality: {
                        required: true,
                        maxlength: 15
                    },
                    date_of_birth: "required",
                    education: "required",
                    mode_of_contact: "required"
                }
            });
        });
    </script>
@endsection
